import React from 'react';
import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:8000/api/',
});
export default class PropertyDetails extends React.Component {
  state = {
    propertyDetails: [],
  };
  componentDidMount() {
    api.get(`properties/1`).then((res) => {
      const propertyDetails = res.data;
      this.setState({ propertyDetails });
      console.log(propertyDetails);
    });
  }
  render() {
    return (
      <>
        <h1>TITLE</h1>
        <div>This is a body.</div>
      </>
    );
  }
}
