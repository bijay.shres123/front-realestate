import React from 'react';

export default function () {
  return (
    <>
      {/* <!-- Topbar --> */}
      <div id="top-bar">
        <div className="container">
          {/* <!-- Left Side Content --> */}
          <div className="left-side">
            {/* <!-- Top bar --> */}
            <ul className="top-bar-menu">
              <li>
                <i className="fa fa-phone"></i> (123) 123-456{' '}
              </li>
              <li>
                <i className="fa fa-envelope"></i>{' '}
                <a href="#">office@example.com</a>
              </li>
              <li>
                <div className="top-bar-dropdown">
                  <span>Dropdown Menu</span>
                  <ul className="options">
                    <li>
                      <div className="arrow"></div>
                    </li>
                    <li>
                      <a href="#">Nice First Link</a>
                    </li>
                    <li>
                      <a href="#">Second Link With Long Title</a>
                    </li>
                    <li>
                      <a href="#">Third Link</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          {/* <!-- Left Side Content / End -->


<!-- Left Side Content --> */}
          <div className="right-side">
            {/* <!-- Social Icons --> */}
            <ul className="social-icons">
              <li>
                <a className="facebook" href="#">
                  <i className="icon-facebook"></i>
                </a>
              </li>
              <li>
                <a className="twitter" href="#">
                  <i className="icon-twitter"></i>
                </a>
              </li>
              <li>
                <a className="gplus" href="#">
                  <i className="icon-gplus"></i>
                </a>
              </li>
              <li>
                <a className="pinterest" href="#">
                  <i className="icon-pinterest"></i>
                </a>
              </li>
            </ul>
          </div>
          {/* <!-- Left Side Content / End --> */}
        </div>
      </div>
      <div className="clearfix"></div>
      {/* <!-- Topbar / End --> */}
    </>
  );
}
