import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

class PropertyCard extends Component<any, any> {
  id = 1;
  render() {
    console.log('Child Component');
    return (
      <>
        <Wrapper>
          <div className="listing-item">
            <a
              href="single-property-page-1.html"
              className="listing-img-container"
            >
              <div className="listing-badges">
                <span className="featured">Featured</span>
                <span>For Sale</span>
              </div>

              <div className="listing-img-content">
                <span className="listing-price">
                  $275,000 <i>$520 / sq ft</i>
                </span>
                <span
                  className="like-icon with-tip"
                  data-tip-content="Add to Bookmarks"
                ></span>
                <span
                  className="compare-button with-tip"
                  data-tip-content="Add to Compare"
                ></span>
              </div>

              <div className="listing-carousel">
                <div>
                  <img src="assets/images/listing-01.jpg" alt="" />
                </div>
              </div>
            </a>

            <div className="listing-content">
              <div className="listing-title">
                <h4>
                  <Link to="/single">Eagle Apartments</Link>
                </h4>
                <a
                  href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                  className="listing-address popup-gmaps"
                >
                  <i className="fa fa-map-marker"></i>
                  9364 School St. Lynchburg, NY
                </a>
              </div>

              <ul className="listing-features">
                <li>
                  Area <span>530 sq ft</span>
                </li>
                <li>
                  Bedrooms <span>2</span>
                </li>
                <li>
                  Bathrooms <span>1</span>
                </li>
              </ul>

              <div className="listing-footer">
                <a href="#">
                  <i className="fa fa-user"></i> David Strozier
                </a>
                <span>
                  <i className="fa fa-calendar-o"></i> 1 day ago
                </span>
              </div>
            </div>
          </div>
        </Wrapper>
        {/* <!-- Listing Item / End --> */}
      </>
    );
  }
}
export default PropertyCard;

const Wrapper = styled.div`
  width: auto;
  display: block;
`;
