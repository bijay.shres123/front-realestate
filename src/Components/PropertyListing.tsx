import React from 'react';
import PropertyCard from '../Components/PropertyCard';
import styled from 'styled-components';

export default class PropertyListing extends React.Component {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <>
        {/* <!-- Content
================================================== --> */}
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h3 className="headline margin-bottom-25 margin-top-65">
                Newly Added
              </h3>
            </div>
          </div>
          {/* <!-- Carousel --> */}
          <div className="row">
            <div className="col-md-4">
              <PropertyCard />
            </div>
            <div className="col-md-4">
              <PropertyCard />
            </div>
            <div className="col-md-4">
              <PropertyCard />
            </div>
          </div>

          {/* <!-- Listing Item --> */}

          {/* <!-- Carousel / End --> */}
        </div>
      </>
    );
  }
}
const Carosuel1 = styled.div`
  width: 403px;
`;
