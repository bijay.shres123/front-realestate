import React from 'react';

const Banner = () => {
  return (
    <>
      {/* <!-- Banner */}
      {/* ================================================== --> */}
      <div
        className="parallax"
        data-color="#36383e"
        data-color-opacity="0.45"
        data-img-width="2500"
        data-img-height="1600"
      >
        <div className="parallax-content">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                {/* <!-- Main Search Container --> */}
                <div className="main-search-container">
                  <h2>Find Your Dream Home</h2>

                  {/* <!-- Main Search --> */}
                  <form className="main-search-form" method="post">
                    {/* <!-- Type --> */}
                    <div className="search-type">
                      <label className="active">
                        <input className="first-tab" name="tab" type="radio" />
                        Any Status
                      </label>
                      <label>
                        <input name="tab" type="radio" />
                        For Sale
                      </label>
                      <label>
                        <input name="tab" type="radio" />
                        For Rent
                      </label>
                      <div className="search-type-arrow"></div>
                    </div>

                    {/* <!-- Box --> */}
                    <div className="main-search-box">
                      {/* <!-- Main Search Input --> */}
                      <div className="main-search-input larger-input">
                        <input
                          type="text"
                          className="ico-01"
                          id="autocomplete-input"
                          placeholder="Enter address e.g. street, city and state or zip"
                          value=""
                        />
                        <button className="button" type="submit">
                          Search
                        </button>
                      </div>

                      {/* <!-- Row --> */}
                      <div className="row with-forms">
                        {/* <!-- Property Type --> */}
                        <div className="col-md-4">
                          <select
                            data-placeholder="Any Type"
                            className="chosen-select-no-single"
                          >
                            <option>Any Type</option>
                            <option>Apartments</option>
                            <option>Houses</option>
                            <option>Commercial</option>
                            <option>Garages</option>
                            <option>Lots</option>
                          </select>
                        </div>

                        {/* <!-- Min Price --> */}
                        <div className="col-md-4">
                          {/* <!-- Select Input --> */}
                          <div className="select-input">
                            <input
                              type="text"
                              placeholder="Min Price"
                              data-unit="USD"
                            />
                          </div>
                          {/* <!-- Select Input / End --> */}
                        </div>

                        {/* <!-- Max Price --> */}
                        <div className="col-md-4">
                          {/* <!-- Select Input --> */}
                          <div className="select-input">
                            <input
                              type="text"
                              placeholder="Max Price"
                              data-unit="USD"
                            />
                          </div>
                          {/* <!-- Select Input / End --> */}
                        </div>
                      </div>
                      {/* <!-- Row / End --> */}

                      {/* <!-- More Search Options --> */}
                      <a
                        href="#"
                        className="more-search-options-trigger"
                        data-open-title="More Options"
                        data-close-title="Less Options"
                      ></a>

                      <div className="more-search-options">
                        <div className="more-search-options-container">
                          {/* <!-- Row --> */}
                          <div className="row with-forms">
                            {/* <!-- Min Price --> */}
                            <div className="col-md-6">
                              {/* <!-- Select Input --> */}
                              <div className="select-input">
                                <input
                                  type="text"
                                  placeholder="Min Area"
                                  data-unit="Sq Ft"
                                />
                              </div>
                              {/* <!-- Select Input / End --> */}
                            </div>

                            {/* <!-- Max Price --> */}
                            <div className="col-md-6">
                              {/* <!-- Select Input --> */}
                              <div className="select-input">
                                <input
                                  type="text"
                                  placeholder="Max Area"
                                  data-unit="Sq Ft"
                                />
                              </div>
                              {/* <!-- Select Input / End --> */}
                            </div>
                          </div>
                          {/* <!-- Row / End --> */}

                          {/* <!-- Row --> */}
                          <div className="row with-forms">
                            {/* <!-- Min Area --> */}
                            <div className="col-md-6">
                              <select
                                data-placeholder="Beds"
                                className="chosen-select-no-single"
                              >
                                <option label="blank"></option>
                                <option>Beds (Any)</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </select>
                            </div>

                            {/* <!-- Max Area --> */}
                            <div className="col-md-6">
                              <select
                                data-placeholder="Baths"
                                className="chosen-select-no-single"
                              >
                                <option label="blank"></option>
                                <option>Baths (Any)</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </select>
                            </div>
                          </div>
                          {/* <!-- Row / End --> */}

                          {/* <!-- Checkboxes --> */}
                          <div className="checkboxes in-row">
                            <input id="check-2" type="checkbox" name="check" />
                            <label htmlFor="check-2">Air Conditioning</label>

                            <input id="check-3" type="checkbox" name="check" />
                            <label htmlFor="check-3">Swimming Pool</label>

                            <input id="check-4" type="checkbox" name="check" />
                            <label htmlFor="check-4">Central Heating</label>

                            <input id="check-5" type="checkbox" name="check" />
                            <label htmlFor="check-5">Laundry Room</label>

                            <input id="check-6" type="checkbox" name="check" />
                            <label htmlFor="check-6">Gym</label>

                            <input id="check-7" type="checkbox" name="check" />
                            <label htmlFor="check-7">Alarm</label>

                            <input id="check-8" type="checkbox" name="check" />
                            <label htmlFor="check-8">Window Covering</label>
                          </div>
                          {/* <!-- Checkboxes / End --> */}
                        </div>
                      </div>
                      {/* <!-- More Search Options / End --> */}
                    </div>
                    {/* <!-- Box / End --> */}
                  </form>
                  {/* <!-- Main Search --> */}
                </div>
                {/* <!-- Main Search Container / End --> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Banner;
