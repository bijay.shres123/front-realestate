import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';

interface Props {
  email: string;
  password: string;
  touched: any;
  errors: any;
}

function LoginBox(props: Props) {
  const { touched, errors } = props;
  return (
    <>
      {/* <!-- Container --> */}
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-md-offset-4">
            <button className="button social-login via-twitter">
              <i className="fa fa-twitter"></i> Log In With Twitter
            </button>
            <button className="button social-login via-gplus">
              <i className="fa fa-google-plus"></i> Log In With Google Plus
            </button>
            <button className="button social-login via-facebook">
              <i className="fa fa-facebook"></i> Log In With Facebook
            </button>

            {/* <!--Tab --> */}
            <div className="my-account style-1 margin-top-5 margin-bottom-40">
              <ul className="tabs-nav">
                <li className="">
                  <a href="#tab1">Log In</a>
                </li>
                <li>
                  <a href="#tab2">Register</a>
                </li>
              </ul>

              <div className="tabs-container alt">
                {/* <!-- Login --> */}
                <div
                  className="tab-content"
                  id="tab1"
                  style={{ display: 'block' }}
                >
                  <Form className="form-container">
                    <div className="form-group ">
                      <br />
                      <br />
                      <label htmlFor="email">Email</label>
                      <Field
                        type="text"
                        name="email"
                        className={'form-control'}
                        placeholder="Email"
                      />
                      {touched.email && errors.email && (
                        <span className="help-block text-danger">
                          {errors.email}
                        </span>
                      )}
                    </div>
                    <div className="form-group">
                      <label htmlFor="password">Password</label>
                      <Field
                        type="password"
                        name="password"
                        className={'form-control'}
                        placeholder="Password"
                      />
                      {touched.password && errors.password && (
                        <span className="help-block text-danger">
                          {errors.password}
                        </span>
                      )}
                    </div>

                    <p className="form-row">
                      <input
                        type="submit"
                        className="button border margin-top-10"
                        name="login"
                        value="Login"
                      />
                    </p>

                    <p className="lost_password">
                      <a href="#">Lost Your Password?</a>
                    </p>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
const LoginFormik = withFormik({
  mapPropsToValues: (props: Props) => {
    return {
      email: props.email || '',
      password: props.password || '',
    };
  },
  validationSchema: Yup.object().shape({
    email: Yup.string().email('Email not valid').required('Email is required'),
    password: Yup.string().required('Password is required'),
  }),
  handleSubmit: (values) => {
    // const REST_API_URL = 'YOUR_REST_API_URL';
    // fetch(REST_API_URL, {
    //   method: 'post',
    //   body: JSON.stringify(values),
    // })
    //   .then((response) => {
    //     if (response.ok) {
    //       return response.json();
    //     } else {
    //       // HANDLE ERROR
    //       throw new Error('Something went wrong');
    //     }
    //   })
    //   .then((data) => {
    //     // HANDLE RESPONSE DATA
    //     console.log(data);
    //   })
    //   .catch((error) => {
    //     // HANDLE ERROR
    //     console.log(error);
    //   });
    alert(values.email);
  },
})(LoginBox);

export default LoginFormik;
