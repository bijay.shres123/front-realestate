import React from 'react';

export default function TitleBar() {
  return (
    <>
      {/* <!-- Titlebar
================================================== --> */}
      <div id="titlebar">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h2>Log In & Register</h2>

              {/* <!-- Breadcrumbs --> */}
              <nav id="breadcrumbs">
                <ul>
                  <li>
                    <a href="#">Home</a>
                  </li>
                  <li>Log In & Register</li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
