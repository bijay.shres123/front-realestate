import React from 'react';
import Topbar from '../Components/Topbar';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import PropertyDetails from '../Components/PropertyDetails';

export default function SingleProperty() {
  return (
    <>
      <Topbar />
      <Navbar />
      <PropertyDetails />
      <Footer />
    </>
  );
}
