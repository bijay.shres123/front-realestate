import React, { Component } from 'react';
import Navbar from '../Components/Navbar';
import Topbar from '../Components/Topbar';
import Footer from '../Components/Footer';
import Banner from '../Components/Banner';
import PropertyListing from '../Components/PropertyListing';

class Home extends Component {
  render() {
    return (
      <>
        {/* <!-- Header Container
================================================== --> */}
        <header id="header-container">
          <Topbar />
          <Navbar />
        </header>
        <Banner />
        <PropertyListing />
        <div className="clearfix"></div>
        {/* <!-- Header Container / End --> */}
        <Footer />
      </>
    );
  }
}

export default Home;
