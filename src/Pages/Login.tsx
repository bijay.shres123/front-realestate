import React from 'react';
import Topbar from '../Components/Topbar';
import Navbar from '../Components/Navbar';
import TitleBar from '../Components/TitleBar';
import LoginBox from '../Components/LoginBox';
import Footer from '../Components/Footer';

interface Props {
  email: string;
  password: string;
  touched: any;
  errors: any;
}
const props: Props = {
  email: '',
  password: '',
  touched: '',
  errors: '',
};
const LoginPage = () => {
  return (
    <>
      <Topbar />
      <Navbar />
      <TitleBar />
      <LoginBox {...props} />
      <Footer />
    </>
  );
};

export default LoginPage;
