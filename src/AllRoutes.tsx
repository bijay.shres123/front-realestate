import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Pages/Home';
import Login from './Pages/Login';
import SingleProperty from './Pages/SingleProperty';

export default function AllRoutes() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/single" component={SingleProperty} />
    </Switch>
  );
}
